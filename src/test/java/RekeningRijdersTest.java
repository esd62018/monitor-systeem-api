import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;


public class RekeningRijdersTest {

//  @Test
//  public void TestGetAllOwners() throws ClientProtocolException, IOException {
//    // Given
//    final String jsonMimeType = "application/json";
//    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12003/OverheidsAdministratie/api/invoices/1");
//    request.addHeader("user-agent", "Postman");
//
//    // When
//    final HttpResponse response = HttpClientBuilder.create().build().execute(request);
//
//    // Then
//    final String mimeType = ContentType.getOrDefault(response.getEntity()).getMimeType();
//    assertEquals(jsonMimeType, mimeType);
//  }
//
//  @Test
//  public void TestGetOwner() throws ClientProtocolException, IOException {
//    // Given
//    final String jsonMimeType = "application/json";
//    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12002/RekeningRijders/api/owners/1");
//
//    // When
//    final HttpResponse response = HttpClientBuilder.create().build().execute(request);
//
//    // Then
//    final String mimeType = ContentType.getOrDefault(response.getEntity()).getMimeType();
//    assertEquals(jsonMimeType, mimeType);
//  }
//
//  @Test
//  public void TestByLicense() throws ClientProtocolException, IOException {
//    // Given
//    final String jsonMimeType = "application/json";
//    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12002/RekeningRijders/api/owners/by-license-plate/31-CD-06");
//
//    // When
//    final HttpResponse response = HttpClientBuilder.create().build().execute(request);
//
//    // Then
//    final String mimeType = ContentType.getOrDefault(response.getEntity()).getMimeType();
//    assertEquals(jsonMimeType, mimeType);
//  }
//
//  @Test
//  public void TestUpdateAccount() throws ClientProtocolException, IOException {
//    // Given
//    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12002/RekeningRijders/api/accounts");
//
//    // When
//    final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
//
//    // Then
//    assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_METHOD_NOT_ALLOWED));
//  }
//
//  @Test
//  public void TestDeleteAccount() throws ClientProtocolException, IOException {
//    // Given
//    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12002/RekeningRijders/api/accounts/1");
//
//    // When
//    final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
//
//    // Then
//    assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_METHOD_NOT_ALLOWED));
//  }
//
//  @Test
//  public void TestLogin() throws ClientProtocolException, IOException {
//    // Given
//    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12002/RekeningRijders/api/auth/login");
//
//    // When
//    final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
//
//    // Then
//    assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_METHOD_NOT_ALLOWED));
//  }
//
//  @Test
//  public void TestSignup() throws ClientProtocolException, IOException {
//    // Given
//    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12002/RekeningRijders/api/auth/sign-up");
//
//    // When
//    final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
//
//    // Then
//    assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_METHOD_NOT_ALLOWED));
//  }
//
//  @Test
//  public void TestInvoicesUser() throws ClientProtocolException, IOException {
//    // Given
//    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12002/RekeningRijders/api/invoices/1");
//
//    // When
//    final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
//
//    // Then
//    assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_FORBIDDEN));
//  }
//
//  @Test
//  public void TestRates() throws ClientProtocolException, IOException {
//    // Given
//    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12002/RekeningRijders/api/rates");
//
//    // When
//    final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
//
//    // Then
//    assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_FORBIDDEN));
//  }
//
//  @Test
//  public void TestVehicleLicense() throws ClientProtocolException, IOException {
//    // Given
//    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12003/OverheidsAdministratie/api/invoices/11111");
//    request.addHeader("user-agent", "Postman");
//
//    // When
//    final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
//
//    // Then
//    assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(200));
//  }
}
