package utils;

import domain.PingResultStatus;

import java.io.IOException;
import java.net.InetAddress;

public class Ping {
  public static PingResultStatus testIpAddress(String ipAddress) throws IOException {
    InetAddress inet = InetAddress.getByName(ipAddress);

    if (inet.isReachable(2000)) {
      return PingResultStatus.AVAILABLE;
    }

    return PingResultStatus.NOT_AVAILABLE;
  }
}
