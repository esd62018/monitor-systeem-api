package utils;

public class Constants {

  // TODO: change URLS
//  public static final String GOVERNMENT_API_URL = "http://localhost:56938/OverheidsAdministratie/api";
//  public static final String DRIVERS_API_URL = "http://localhost:8080/RekeningRijders/api";
//  public static final String REGISTRATION_API_URL = "http://localhost:8080/RegistratieSysteem/api";
//  public static final String POLICE_API_URL = "http://localhost:8080/popoz/api";

  public static final String GOVERNMENT_API_URL = "http://192.168.24.80:12003/OverheidsAdministratie/api";
  public static final String DRIVERS_API_URL = "http://192.168.24.80:12002/RekeningRijders/api";
  public static final String REGISTRATION_API_URL = "http://192.168.24.80:12004/VerplaatsingRegistratie/api";
  public static final String POLICE_API_URL = "http://192.168.24.80:12001/popoz/api";

  public static final String GERMANY_URL = "8.8.8.8";
  public static final String THE_NETHERLANDS_URL = "8.8.8.8";
  public static final String FINLAND_URL = "8.8.8.8";
  public static final String ITALY_URL = "8.8.8.8";

  public static final String JWS_SECRET = "SecretKey333";
}
