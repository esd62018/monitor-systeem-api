package filter;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import utils.Config;
import utils.Constants;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

@WebFilter(
  filterName = "AuthFilter",
  urlPatterns = {
    "/api/drivers-test-rest/*",
    "/api/government-test-rest/*",
    "/api/police-test-rest/*",
    "/api/registration-test-rest/*"
  })
public class AuthFilter implements Filter {
  private final String TRUSTED_API_TOKEN = Config.getInstance().getProperties().getProperty("api-token");

  @Override
  public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws ServletException, IOException {
    HttpServletRequest request = (HttpServletRequest) req;
    HttpServletResponse response = (HttpServletResponse) res;

    String token = request.getHeader("token");

    // Check if resource request, from 'trusted' backend or from postman -> allow
    if (request.getRequestURI().contains("javax.faces.resource")
      || (token != null && token.equals(TRUSTED_API_TOKEN))
      || getIsRequestFromPostMan(request)
      ) {
      chain.doFilter(request, response);

      return;
    }

    if (token == null) {
      response.sendError(403);

      return;
    }

    try {
      // OK, we can trust this JWT
      Jwts.parser().setSigningKey(Constants.JWS_SECRET).parseClaimsJws(token);

      chain.doFilter(request, response);
    } catch (SignatureException | MalformedJwtException | ExpiredJwtException e) {
      // Don't trust the JWT!
      System.out.println("JWT e: " + e.toString());
      response.sendError(403);
    }
  }

  private boolean getIsRequestFromPostMan(HttpServletRequest request) {
    boolean requestIsFromPostman = false;

    Enumeration headerNames = request.getHeaderNames();
    while (headerNames.hasMoreElements()) {
      String key = (String) headerNames.nextElement();
      String value = request.getHeader(key);

      // Check if request is coming from postman
      if (key.equals("user-agent") && value.contains("Postman")) {
        requestIsFromPostman = true;

        break;
      }
    }

    return requestIsFromPostman;
  }
}
