package rest;

import domain.TestResult;
import utils.Config;
import utils.Constants;
import utils.Error;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/registration-test-rest")
public class RegistrationApiTestRest {
  private final String TRUSTED_API_TOKEN = Config.getInstance().getProperties().getProperty("api-token");

  private Client client;

  public RegistrationApiTestRest() {
    client = ClientBuilder.newClient();
  }

  @GET
  @Produces({ APPLICATION_JSON })
  public Response getTestResults() {
    List<TestResult> testResults = new ArrayList<TestResult>();
    TestResult testGetAllJourneyResult = new TestResult();
    TestResult testGetAllJourneysFromSerialNumber = new TestResult();
    TestResult testGetTranslocationById = new TestResult();
    TestResult testGetTrackIds = new TestResult();

    // Test get all journeys
    try {
      // Fetch
      Response response = client.target(Constants.REGISTRATION_API_URL)
        .path("journeys")
        .request(MediaType.APPLICATION_JSON)
        .header("token", TRUSTED_API_TOKEN)
        .get();

      // Set test results
      testGetAllJourneyResult.setName("Get all journeys");
      testGetAllJourneyResult.setResult(response.readEntity(String.class));
      testGetAllJourneyResult.setStatus(response.getStatus());
    } catch (Exception e) {
      e.printStackTrace();

      // Set test error
      testGetAllJourneyResult.setStatus(500);
      testGetAllJourneyResult.setError(Error.getStackTrace(e));
    } finally {
      // Add test results to list of results
      testResults.add(testGetAllJourneyResult);
    }

    // Test get all journeys from serial where date between
    try {
      // Fetch
      Response response = client.target(Constants.REGISTRATION_API_URL)
        .path("journeys/d1da42aa-f658-4d1d-8f37-933b9f7e7817/2018-06-02/2018-06-04")
        .request(MediaType.APPLICATION_JSON)
        .header("token", TRUSTED_API_TOKEN)
        .get();

      // Set test results
      testGetAllJourneysFromSerialNumber.setName("Get all journeys from serial number");
      testGetAllJourneysFromSerialNumber.setResult(response.readEntity(String.class));
      testGetAllJourneysFromSerialNumber.setStatus(response.getStatus());
    } catch (Exception e) {
      e.printStackTrace();

      // Set test error
      testGetAllJourneysFromSerialNumber.setStatus(500);
      testGetAllJourneysFromSerialNumber.setError(Error.getStackTrace(e));
    } finally {
      // Add test results to list of results
      testResults.add(testGetAllJourneysFromSerialNumber);
    }

    // Test get translocation by id
    try {
      // Fetch
      Response response = client.target(Constants.REGISTRATION_API_URL)
        .path("translocation/1")
        .request(MediaType.APPLICATION_JSON)
        .header("token", TRUSTED_API_TOKEN)
        .get();

      // Set test results
      testGetTranslocationById.setName("Get translocation by id");
      testGetTranslocationById.setResult(response.readEntity(String.class));
      testGetTranslocationById.setStatus(response.getStatus());
    } catch (Exception e) {
      e.printStackTrace();

      // Set test error
      testGetTranslocationById.setStatus(500);
      testGetTranslocationById.setError(Error.getStackTrace(e));
    } finally {
      // Add test results to list of results
      testResults.add(testGetTranslocationById);
    }

    // Test get all track ids
    try {
      // Fetch
      Response response = client.target(Constants.REGISTRATION_API_URL)
        .path("police/track")
        .request(MediaType.APPLICATION_JSON)
        .header("token", TRUSTED_API_TOKEN)
        .get();

      // Set test results
      testGetTrackIds.setName("Get track ids");
      testGetTrackIds.setResult(response.readEntity(String.class));
      testGetTrackIds.setStatus(response.getStatus());
    } catch (Exception e) {
      e.printStackTrace();

      // Set test error
      testGetTrackIds.setStatus(500);
      testGetTrackIds.setError(Error.getStackTrace(e));
    } finally {
      // Add test results to list of results
      testResults.add(testGetTrackIds);
    }

    return Response
      .status(200)
      .entity(testResults)
      .build();
  }
}
