package rest;

import domain.TestResult;
import utils.Config;
import utils.Constants;
import utils.Error;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/drivers-test-rest")
public class DiversApiTestRest {
  private final String TRUSTED_API_TOKEN = Config.getInstance().getProperties().getProperty("api-token");

  private Client client;

  public DiversApiTestRest() {
    client = ClientBuilder.newClient();
  }

  @GET
  @Produces({ APPLICATION_JSON })
  public Response getTestResults() {
    List<TestResult> testResults = new ArrayList<TestResult>();
    TestResult testGetVehicleHistoriesResult = new TestResult();
    TestResult testGetOwnersResult = new TestResult();

    // Test get vehicle histories
    try {
      // Fetch
      Response response = client.target(Constants.DRIVERS_API_URL)
        .path("vehicle-histories")
        .request(MediaType.APPLICATION_JSON)
        .header("token", TRUSTED_API_TOKEN)
        .get();

      // Set test results
      testGetVehicleHistoriesResult.setName("Get vehicle histories");
      testGetVehicleHistoriesResult.setResult(response.readEntity(String.class));
      testGetVehicleHistoriesResult.setStatus(response.getStatus());
    } catch (Exception e) {
      e.printStackTrace();

      // Set test error
      testGetVehicleHistoriesResult.setStatus(500);
      testGetVehicleHistoriesResult.setError(Error.getStackTrace(e));
    } finally {
      // Add test results to list of results
      testResults.add(testGetVehicleHistoriesResult);
    }

    // Test get owners
    try {
      // Fetch
      Response response = client.target(Constants.DRIVERS_API_URL)
        .path("owners")
        .request(MediaType.APPLICATION_JSON)
        .header("token", TRUSTED_API_TOKEN)
        .get();

      // Set test results
      testGetOwnersResult.setName("Get vehicle histories");
      testGetOwnersResult.setResult(response.readEntity(String.class));
      testGetOwnersResult.setStatus(response.getStatus());
    } catch (Exception e) {
      e.printStackTrace();

      // Set test error
      testGetOwnersResult.setStatus(500);
      testGetOwnersResult.setError(Error.getStackTrace(e));
    } finally {
      // Add test results to list of results
      testResults.add(testGetOwnersResult);
    }

    return Response
      .status(200)
      .entity(testResults)
      .build();
  }
}
