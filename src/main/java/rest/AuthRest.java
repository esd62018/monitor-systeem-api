package rest;


import domain.User;
import dto.UserDto;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import service.UserService;
import utils.Constants;

import javax.ejb.EJBTransactionRolledbackException;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/auth")
public class AuthRest {
  private static final String DATE_FORMAT = "yyyy/MM/dd HH:mm:ss";
  private static final DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);

  @Inject
  private UserService userService;

  public AuthRest() {

  }

  /**
   * Route to authenticate user
   */
  @POST
  @Path("/login")
  @Consumes({ APPLICATION_JSON })
  @Produces({ APPLICATION_JSON })
  public Response authenticate(User user) {
    ResponseBody responseBody = new ResponseBody();
    int status = 200;

    try {
      UserDto userDto = UserDto.fromUser(userService.authenticate(user));
      LocalDateTime localDateTime = new Date()
        .toInstant()
        .atZone(ZoneId.systemDefault())
        .toLocalDateTime()
        .plusDays(3);

      Date expiration = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());

      String token = Jwts.builder()
        .setExpiration(expiration)
        .setSubject(String.valueOf(userDto.getId()))
        .signWith(SignatureAlgorithm.HS512, Constants.JWS_SECRET)
        .compact();

      responseBody.setAuth(userDto);
      responseBody.setExpireDate(dateFormat.format(expiration));
      responseBody.setToken(token);

    } catch (Exception e) {
      if (e instanceof EJBTransactionRolledbackException) {   // Caused by no account found
        status = 404;
      } else {
        status = 500;
      }
    }

    return Response
      .status(status)
      .entity(responseBody)
      .build();
  }

  public class ResponseBody {
    private UserDto auth;
    private String token;
    private String expireDate;

    public ResponseBody() {

    }

    public UserDto getAuth() {
      return auth;
    }

    public void setAuth(UserDto auth) {
      this.auth = auth;
    }

    public String getToken() {
      return token;
    }

    public void setToken(String token) {
      this.token = token;
    }

    public String getExpireDate() {
      return expireDate;
    }

    public void setExpireDate(String expireDate) {
      this.expireDate = expireDate;
    }
  }
}
