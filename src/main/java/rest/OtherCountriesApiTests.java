package rest;

import domain.PingResultStatus;
import domain.PingTestResult;
import utils.Constants;
import utils.Ping;
import utils.Error;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import java.util.List;
import java.util.ArrayList;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/other-countries-tests")
public class OtherCountriesApiTests {

  public OtherCountriesApiTests() {

  }

  @GET
  @Produces({ APPLICATION_JSON })
  public Response getTestResults() {
    List<PingTestResult> testResultList = new ArrayList<>();
    PingTestResult italyTestResult = new PingTestResult();
    PingTestResult germanyTestResult = new PingTestResult();
    PingTestResult theNetherlandslandsTestResult = new PingTestResult();
    PingTestResult finlandTestResult = new PingTestResult();

    // Test Italy
    try {
      italyTestResult.setStatus(Ping.testIpAddress(Constants.ITALY_URL));
    } catch (Exception e) {
      e.printStackTrace();
      italyTestResult.setStatus(PingResultStatus.ERROR);
      italyTestResult.setError(Error.getStackTrace(e));
    } finally {
      italyTestResult.setName("Italy");

      testResultList.add(italyTestResult);
    }

    // Test Germany
    try {
      germanyTestResult.setStatus(Ping.testIpAddress(Constants.GERMANY_URL));
    } catch (Exception e) {
      e.printStackTrace();
      germanyTestResult.setStatus(PingResultStatus.ERROR);
      germanyTestResult.setError(Error.getStackTrace(e));
    } finally {
      germanyTestResult.setName("Germany");

      testResultList.add(germanyTestResult);
    }

    // Test the Netherlands
    try {
      theNetherlandslandsTestResult.setStatus(Ping.testIpAddress(Constants.THE_NETHERLANDS_URL));
    } catch (Exception e) {
      e.printStackTrace();
      theNetherlandslandsTestResult.setStatus(PingResultStatus.ERROR);
      theNetherlandslandsTestResult.setError(Error.getStackTrace(e));
    } finally {
      theNetherlandslandsTestResult.setName("the Netherlands");

      testResultList.add(theNetherlandslandsTestResult);
    }

    // Test Finland
    try {
      finlandTestResult.setStatus(Ping.testIpAddress(Constants.FINLAND_URL));
    } catch (Exception e) {
      e.printStackTrace();
      finlandTestResult.setStatus(PingResultStatus.ERROR);
      finlandTestResult.setError(Error.getStackTrace(e));
    } finally {
      finlandTestResult.setName("Finland");

      testResultList.add(finlandTestResult);
    }

    return Response
      .status(200)
      .entity(testResultList)
      .build();
  }
}
