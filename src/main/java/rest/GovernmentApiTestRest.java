package rest;

import domain.TestResult;
import utils.Config;
import utils.Constants;
import utils.Error;

import javax.ws.rs.*;
import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.List;
import java.util.ArrayList;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/government-test-rest")
public class GovernmentApiTestRest {
  private final String TRUSTED_API_TOKEN = Config.getInstance().getProperties().getProperty("api-token");

  private Client client;

  public GovernmentApiTestRest () {
    client = ClientBuilder.newClient();
  }

  @GET
  @Produces({ APPLICATION_JSON })
  public Response getTestResults() {
    List<TestResult> testResults = new ArrayList<TestResult>();
    TestResult testGetInvoicesResult = new TestResult();
    TestResult testGetKmRates = new TestResult();

    // Test get invoices
    try {
      // Fetch
      Response response = client.target(Constants.GOVERNMENT_API_URL)
        .path("invoices")
        .request(MediaType.APPLICATION_JSON)
        .header("token", TRUSTED_API_TOKEN)
        .get();

      // Set test results
      testGetInvoicesResult.setName("Get invoices");
      testGetInvoicesResult.setResult(response.readEntity(String.class));
      testGetInvoicesResult.setStatus(response.getStatus());
    } catch (Exception e) {
      e.printStackTrace();

      // Set test error
      testGetInvoicesResult.setStatus(500);
      testGetInvoicesResult.setError(Error.getStackTrace(e));
    } finally {
      // Add test results to list of results
      testResults.add(testGetInvoicesResult);
    }

    // Test get KM rates
    try {
      Response response = client.target(Constants.GOVERNMENT_API_URL)
        .path("rates")
        .request(MediaType.APPLICATION_JSON)
        .header("token", TRUSTED_API_TOKEN)
        .get();

      // Set test results
      testGetKmRates.setName("Get KM ratings");
      testGetKmRates.setResult(response.readEntity(String.class));
      testGetKmRates.setStatus(response.getStatus());
    } catch (Exception e) {
      e.printStackTrace();

      // Set test error
      testGetKmRates.setStatus(500);
      testGetKmRates.setError(Error.getStackTrace(e));
    } finally {
      // Add test results to list of results
      testResults.add(testGetKmRates);
    }

    return Response
      .status(200)
      .entity(testResults)
      .build();
  }
}
