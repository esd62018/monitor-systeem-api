package rest;

import domain.TestResult;
import utils.Config;
import utils.Constants;
import utils.Error;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/police-test-rest")
public class PoliceApiTestRest {
  private final String TRUSTED_API_TOKEN = Config.getInstance().getProperties().getProperty("api-token");

  private Client client;

  public PoliceApiTestRest() {
    client = ClientBuilder.newClient();
  }

  @GET
  @Produces({ APPLICATION_JSON })
  public Response getTestResults() {
    List<TestResult> testResults = new ArrayList<TestResult>();
    TestResult testGetAccountByUsername = new TestResult();
    TestResult testGetAccountById = new TestResult();
    TestResult testGetAllAccounts = new TestResult();

    // Test get account by username
    try {
      // Fetch
      Response response = client.target(Constants.POLICE_API_URL)
        .path("police/byUserName/PolitieBas")
        .request(MediaType.APPLICATION_JSON)
        .header("token", TRUSTED_API_TOKEN)
        .get();

      // Set test results
      testGetAccountByUsername.setName("Get all get account by username");
      testGetAccountByUsername.setResult(response.readEntity(String.class));
      testGetAccountByUsername.setStatus(response.getStatus());
    } catch (Exception e) {
      e.printStackTrace();

      // Set test error
      testGetAccountByUsername.setStatus(500);
      testGetAccountByUsername.setError(Error.getStackTrace(e));
    } finally {
      // Add test results to list of results
      testResults.add(testGetAccountByUsername);
    }

    // Test get account by id
    try {
      // Fetch
      Response response = client.target(Constants.POLICE_API_URL)
        .path("police/byID/1")
        .request(MediaType.APPLICATION_JSON)
        .header("token", TRUSTED_API_TOKEN)
        .get();

      // Set test results
      testGetAccountById.setName("Get all get account by id");
      testGetAccountById.setResult(response.readEntity(String.class));
      testGetAccountById.setStatus(response.getStatus());
    } catch (Exception e) {
      e.printStackTrace();

      // Set test error
      testGetAccountById.setStatus(500);
      testGetAccountById.setError(Error.getStackTrace(e));
    } finally {
      // Add test results to list of results
      testResults.add(testGetAccountById);
    }

    // Test get account by id
    try {
      // Fetch
      Response response = client.target(Constants.POLICE_API_URL)
        .path("police/all")
        .request(MediaType.APPLICATION_JSON)
        .header("token", TRUSTED_API_TOKEN)
        .get();

      // Set test results
      testGetAllAccounts.setName("Get all accounts");
      testGetAllAccounts.setResult(response.readEntity(String.class));
      testGetAllAccounts.setStatus(response.getStatus());
    } catch (Exception e) {
      e.printStackTrace();

      // Set test error
      testGetAllAccounts.setStatus(500);
      testGetAllAccounts.setError(Error.getStackTrace(e));
    } finally {
      // Add test results to list of results
      testResults.add(testGetAllAccounts);
    }


    return Response
      .status(200)
      .entity(testResults)
      .build();
  }
}
