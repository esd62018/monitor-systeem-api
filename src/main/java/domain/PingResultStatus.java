package domain;

public enum PingResultStatus {
  AVAILABLE,
  NOT_AVAILABLE,
  ERROR
}
