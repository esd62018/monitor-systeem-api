package domain;

public class PingTestResult {
  private PingResultStatus status;
  private String name;
  private String error;

  public PingTestResult() {

  }

  public PingResultStatus getStatus() {
    return status;
  }

  public void setStatus(PingResultStatus status) {
    this.status = status;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }
}
