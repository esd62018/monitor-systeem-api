package apps;
import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.HttpClientBuilder;

public class Rijderssysteem {

  public Rijderssysteem() {
  }

  public Boolean TestGetAllOwners() throws IOException {
    // Given
    final String jsonMimeType = "application/json";
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12002/RekeningRijders/api/owners");

    // When
    final HttpResponse response = HttpClientBuilder.create().build().execute(request);

    // Then
    final String mimeType = ContentType.getOrDefault(response.getEntity()).getMimeType();
    return jsonMimeType.equals(mimeType);
  }

  public Boolean TestGetOwner() throws IOException {
    // Given
    final String jsonMimeType = "application/json";
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12002/RekeningRijders/api/owners/1");

    // When
    final HttpResponse response = HttpClientBuilder.create().build().execute(request);

    // Then
    final String mimeType = ContentType.getOrDefault(response.getEntity()).getMimeType();
    return jsonMimeType.equals(mimeType);
  }

  public Boolean TestByLicense() throws IOException {
    // Given
    final String jsonMimeType = "application/json";
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12002/RekeningRijders/api/owners/by-license-plate/31-CD-06");

    // When
    final HttpResponse response = HttpClientBuilder.create().build().execute(request);

    // Then
    final String mimeType = ContentType.getOrDefault(response.getEntity()).getMimeType();
    return jsonMimeType.equals(mimeType);
  }

  public Boolean TestUpdateAccount() throws IOException {
    // Given
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12002/RekeningRijders/api/accounts");

    // When
    final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

    // Then
    return httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_METHOD_NOT_ALLOWED;
  }

  public Boolean TestDeleteAccount() throws IOException {
    // Given
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12002/RekeningRijders/api/accounts/1");

    // When
    final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

    // Then
    return httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_METHOD_NOT_ALLOWED;
  }

  public Boolean TestLogin() throws IOException {
    // Given
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12002/RekeningRijders/api/auth/login");

    // When
    final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

    // Then
    return httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_METHOD_NOT_ALLOWED;
  }

  public Boolean TestSignup() throws IOException {
    // Given
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12002/RekeningRijders/api/auth/sign-up");

    // When
    final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

    // Then
    return httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_METHOD_NOT_ALLOWED;
  }

  public Boolean TestInvoicesUser() throws IOException {
    // Given
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12002/RekeningRijders/api/invoices/1");

    // When
    final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

    // Then
    return httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_FORBIDDEN;
  }

  public Boolean TestRates() throws IOException {
    // Given
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12002/RekeningRijders/api/rates");

    // When
    final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

    // Then
    return httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_FORBIDDEN;
  }

  public Boolean TestVehicleLicense() throws IOException {
    // Given
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12002/RekeningRijders/api/vehicles/by-license-plate/31-CD-06");

    // When
    final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

    // Then
    return httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_FORBIDDEN;
  }

  public Boolean TestVehicleHistories() throws IOException {
    // Given
    final String jsonMimeType = "application/json";
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12002/RekeningRijders/api/vehicle-histories");

    // When
    final HttpResponse response = HttpClientBuilder.create().build().execute(request);

    // Then
    final String mimeTypes = ContentType.getOrDefault(response.getEntity()).getMimeType();
    return jsonMimeType.equals(mimeTypes);
  }

  public Boolean TestOwnerID() throws IOException {
    // Given
    final String jsonMimeType = "application/json";
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12002/RekeningRijders/api/vehicle-histories/1");

    // When
    final HttpResponse response = HttpClientBuilder.create().build().execute(request);

    // Then
    final String mimeType = ContentType.getOrDefault(response.getEntity()).getMimeType();
    return jsonMimeType.equals(mimeType);
  }
}
