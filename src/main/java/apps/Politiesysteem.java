package apps;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;

public class Politiesysteem {

  public Politiesysteem() {
  }

  public Boolean TestByUsername() throws IOException {
    // Given
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12001/popoz/api/police/byUserName/erterer");

    // When
    final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

    // Then
    return httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_INTERNAL_SERVER_ERROR;
  }

  public Boolean TestByID() throws IOException {
    // Given
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12001/popoz/api/police/byID/111");

    // When
    final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

    // Then
    return httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_INTERNAL_SERVER_ERROR;
  }

  public Boolean TestGetAll() throws IOException {
    // Given
    final String jsonMimeType = "application/json";
    HttpUriRequest request = new HttpGet("http://192.168.24.80:12001/popoz/api/police/all");
    request.addHeader("user-agent", "Postman");

    // When
    final HttpResponse response = HttpClientBuilder.create().build().execute(request);

    // Then
    final String mimeType = ContentType.getOrDefault(response.getEntity()).getMimeType();
    return jsonMimeType.equals(mimeType);
  }

  public Boolean TestDelete() throws IOException {
    // Given
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12001/popoz/api/police/delete/111");

    // When
    final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

    // Then
    return httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_INTERNAL_SERVER_ERROR;
  }

  public Boolean TestGetAllVehicles() throws IOException {
    // Given
    final String jsonMimeType = "application/json";
    HttpUriRequest request = new HttpGet("http://192.168.24.80:12001/popoz/api/vehicle/all");
    request.addHeader("user-agent", "Postman");

    // When
    final HttpResponse response = HttpClientBuilder.create().build().execute(request);

    // Then
    final String mimeType = ContentType.getOrDefault(response.getEntity()).getMimeType();
    return jsonMimeType.equals(mimeType);
  }

  public Boolean TestGetAllStolenVehicles() throws IOException {
    // Given
    final String jsonMimeType = "application/json";
    HttpUriRequest request = new HttpGet("http://192.168.24.80:12001/popoz/api/vehicle/stolen");
    request.addHeader("user-agent", "Postman");

    // When
    final HttpResponse response = HttpClientBuilder.create().build().execute(request);

    // Then
    final String mimeType = ContentType.getOrDefault(response.getEntity()).getMimeType();
    return jsonMimeType.equals(mimeType);
  }

  public Boolean TestByLicense() throws IOException {
    // Given
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12001/popoz/api/vehicle/byLicensePlate/AA-AA-AA");

    // When
    final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

    // Then
    return httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_INTERNAL_SERVER_ERROR;
  }

  public Boolean TestReport() throws IOException {
    // Given
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12001/popoz/api/vehicle/report");

    // When
    final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

    // Then
    return httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_FORBIDDEN;
  }

  public Boolean TestLogin() throws IOException {
    // Given
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12001/popoz/auth/login");

    // When
    final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

    // Then
    return httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_METHOD_NOT_ALLOWED;
  }

  public Boolean TestSignup() throws IOException {
    // Given
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12001/popoz/auth/sign-up");

    // When
    final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

    // Then
    return httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_METHOD_NOT_ALLOWED;
  }
}
