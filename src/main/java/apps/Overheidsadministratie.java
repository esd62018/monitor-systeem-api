package apps;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;

public class Overheidsadministratie {

  public Overheidsadministratie() {
  }

  public Boolean TestLogin() throws IOException {
    // Given
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12003/OverheidsAdministratie/api/auth/login");

    // When
    final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

    // Then
    return httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_METHOD_NOT_ALLOWED;
  }

  public Boolean TestSignup() throws IOException {
    // Given
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12003/OverheidsAdministratie/api/auth/sign-up");

    // When
    final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

    // Then
    return httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_METHOD_NOT_ALLOWED;
  }

  public Boolean TestPutAccount() throws IOException {
    // Given
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12003/OverheidsAdministratie/api/accounts");

    // When
    final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

    // Then
    return httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_METHOD_NOT_ALLOWED;
  }

  public Boolean TestDeleteAccount() throws IOException {
    // Given
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12003/OverheidsAdministratie/api/accounts/111");

    // When
    final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

    // Then
    return httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_METHOD_NOT_ALLOWED;
  }

  public Boolean TestGetInvoices() throws IOException {
    // Given
    final String jsonMimeType = "application/json";
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12003/OverheidsAdministratie/api/invoices");
    request.addHeader("user-agent", "Postman");

    // When
    final HttpResponse response = HttpClientBuilder.create().build().execute(request);

    // Then
    final String mimeType = ContentType.getOrDefault(response.getEntity()).getMimeType();
    return jsonMimeType.equals(mimeType);
  }

  public Boolean TestGetInvoicesByOwner() throws IOException {
    // Given
    final String jsonMimeType = "application/json";
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12003/OverheidsAdministratie/api/invoices/1");
    request.addHeader("user-agent", "Postman");

    // When
    final HttpResponse response = HttpClientBuilder.create().build().execute(request);

    // Then
    final String mimeType = ContentType.getOrDefault(response.getEntity()).getMimeType();
    return jsonMimeType.equals(mimeType);
  }

  public Boolean TestDeleteInvoice() throws IOException {
    // Given
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12003/OverheidsAdministratie/api/invoices/11111");
    request.addHeader("user-agent", "Postman");

    // When
    final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

    // Then
    return httpResponse.getStatusLine().getStatusCode() == 200;
  }

  public Boolean TestGetOwners() throws IOException {
    // Given
    final String jsonMimeType = "application/json";
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12003/OverheidsAdministratie/api/owners");
    request.addHeader("user-agent", "Postman");

    // When
    final HttpResponse response = HttpClientBuilder.create().build().execute(request);

    // Then
    final String mimeType = ContentType.getOrDefault(response.getEntity()).getMimeType();
    return jsonMimeType.equals(mimeType);
  }

  public Boolean TestGetVehicleHistories() throws IOException {
    // Given
    final String jsonMimeType = "application/json";
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12003/OverheidsAdministratie/api/vehicle-histories");
    request.addHeader("user-agent", "Postman");

    // When
    final HttpResponse response = HttpClientBuilder.create().build().execute(request);

    // Then
    final String mimeType = ContentType.getOrDefault(response.getEntity()).getMimeType();
    return jsonMimeType.equals(mimeType);
  }

  public Boolean TestGetRates() throws IOException {
    // Given
    final String jsonMimeType = "application/json";
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12003/OverheidsAdministratie/api/rates");
    request.addHeader("user-agent", "Postman");

    // When
    final HttpResponse response = HttpClientBuilder.create().build().execute(request);

    // Then
    final String mimeType = ContentType.getOrDefault(response.getEntity()).getMimeType();
    return jsonMimeType.equals(mimeType);
  }

  public Boolean TestDeleteRates() throws IOException {
    // Given
    final HttpUriRequest request = new HttpGet("http://192.168.24.80:12003/OverheidsAdministratie/api/rates/11111");
    request.addHeader("user-agent", "Postman");

    // When
    final HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

    // Then
    return httpResponse.getStatusLine().getStatusCode() == 200;
  }
}
