package service;

import dao.UserDao;
import domain.User;

import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class UserService {

  @Inject
  private UserDao userDao;

  public UserService () {

  }

  public User authenticate(User user) {
    return userDao.findUserByLoginCredentials(user.getUsername(), user.getPassword());
  }
}
