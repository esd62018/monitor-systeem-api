package dao;

import domain.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class UserDao {
  @PersistenceContext
  private EntityManager entityManager;

  public UserDao () {

  }

  public User findUserByLoginCredentials(String username, String password) {
    return entityManager
      .createNamedQuery("User.auth", User.class)
      .setParameter("username", username)
      .setParameter("password", password)
      .getSingleResult();
  }
}
